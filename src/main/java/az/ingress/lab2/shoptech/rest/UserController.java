package az.ingress.lab2.shoptech.rest;

import az.ingress.lab2.shoptech.dto.UserRequestDTO;
import az.ingress.lab2.shoptech.dto.UserResponseDTO;
import az.ingress.lab2.shoptech.respository.UserRepository;
import az.ingress.lab2.shoptech.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    public UserResponseDTO createUser(@RequestBody @Validated UserRequestDTO userRequestDTO) {
        return userService.createUser(userRequestDTO);
    }

    @GetMapping("get-by-id/{id}")
    public UserResponseDTO getUserByID(@PathVariable Long id) {
        return userService.getUserByID(id);
    }

    @PutMapping("/update-by-id/{id}")
    public UserResponseDTO updateUser(@PathVariable Long id,
                                      @RequestBody UserRequestDTO userRequestDTO) {
        return userService.updateUser(id, userRequestDTO);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }

    @GetMapping
    public Page<UserResponseDTO> getUserList(Pageable pageable) {
        return userService.getUserList(pageable);
    }

    @PutMapping("update-is-active/{id}")
    public UserResponseDTO updateUserIsActive(@PathVariable Long id,
                                              @RequestBody UserRequestDTO userRequestDTO){
        return userService.updateUserIsActive(id,userRequestDTO);
    }

//    @GetMapping("/get-is-active/{isActive}")
//    public List<UserResponseDTO> getIsActiveUsers(@PathVariable Boolean isActive){
//        return userService.getIsActiveUser(isActive);
//    }

}
