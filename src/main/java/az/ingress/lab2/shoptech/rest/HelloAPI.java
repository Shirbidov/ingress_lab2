package az.ingress.lab2.shoptech.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
    @RequestMapping("/hello")
    public class HelloAPI {

    @GetMapping
    public String hello(){
        return "Hello API!";
    }

}
