package az.ingress.lab2.shoptech.rest;

import az.ingress.lab2.shoptech.dto.ProductRequestDTO;
import az.ingress.lab2.shoptech.dto.ProductResponseDTO;
import az.ingress.lab2.shoptech.respository.ProductRepository;
import az.ingress.lab2.shoptech.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @PostMapping
    public ProductResponseDTO createProduct(@RequestBody @Validated ProductRequestDTO productRequestDTO){
        return productService.createProduct(productRequestDTO);
    }

    @GetMapping("/{id}")
    public ProductResponseDTO getProductByID(@PathVariable Long id){
        return productService.getProductByID(id);
    }

    @PutMapping("/{id}")
    public ProductResponseDTO updateProduct(@PathVariable Long id, @RequestBody ProductRequestDTO requestDTO){
        return productService.updateProduct(id,requestDTO);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable Long id){
        productService.deleteProduct(id);
    }
    @GetMapping
    public Page<ProductResponseDTO> getProductList(Pageable pageable){
        return productService.getProductList(pageable);
    }

    @GetMapping("/get-by-name/{name}")
    public ProductResponseDTO getProductByName(@PathVariable String name){
      return   productService.getProductByName(name);
    }



}
