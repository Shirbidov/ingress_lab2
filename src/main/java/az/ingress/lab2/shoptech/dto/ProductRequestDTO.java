package az.ingress.lab2.shoptech.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductRequestDTO {

    private String name;
    private Double price;
    private Integer stockCount;
}
