package az.ingress.lab2.shoptech.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserResponseDTO {
    private Long id;
    private String username;
    private String name;
    private String surname;
    private Integer age;
    private Double balance;
    private Boolean isActive;
}
