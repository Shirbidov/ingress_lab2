package az.ingress.lab2.shoptech.service;

import az.ingress.lab2.shoptech.domain.UserEntity;
import az.ingress.lab2.shoptech.dto.UserRequestDTO;
import az.ingress.lab2.shoptech.dto.UserResponseDTO;
import az.ingress.lab2.shoptech.exaption.NotFoundException;
import az.ingress.lab2.shoptech.respository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;


    @Override
    public UserResponseDTO createUser(UserRequestDTO userRequestDTO) {
        UserEntity userEntity =
                UserEntity
                        .builder()
                        .username(userRequestDTO.getUsername())
                        .name(userRequestDTO.getName())
                        .surname(userRequestDTO.getSurname())
                        .age(userRequestDTO.getAge())
                        .balance(userRequestDTO.getBalance())
                        .enable(userRequestDTO.getIsActive())
                        .build();
        final UserEntity userSaved = userRepository.save(userEntity);
        return UserResponseDTO
                .builder()
                .id(userSaved.getId())
                .username(userEntity.getUsername())
                .name(userEntity.getName())
                .surname(userEntity.getSurname())
                .age(userEntity.getAge())
                .balance(userEntity.getBalance())
                .isActive(userEntity.getEnable())
                .build();
    }

    @Override
    public UserResponseDTO getUserByID(Long id) {
        final UserEntity userEntity = userRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        return UserResponseDTO
                .builder()
                .id(userEntity.getId())
                .username(userEntity.getUsername())
                .name(userEntity.getName())
                .surname(userEntity.getSurname())
                .age(userEntity.getAge())
                .balance(userEntity.getBalance())
                .isActive(userEntity.getEnable())
                .build();
    }


//    @Override
//    public UserResponseDTO getUserByIsActive(Boolean isActive) {
//          return null;//  userRepository.findByUserEnable(isActive).stream().map(user ->
////                  UserResponseDTO
////                          .builder()
////                          .id(user.)
////                          .username(user.getUsername())
////                          .name(user.getName())
////                          .surname(user.getSurname())
////                          .balance(user.getBalance())
////                          .age(user.getAge())
////                          .isActive(user.getEnable())
////                          .build());
//
//    }


    @Override
    public UserResponseDTO updateUser(Long id, UserRequestDTO userRequestDTO) {
        UserEntity userEntity =
                UserEntity
                        .builder()
                        .id(id)
                        .username(userRequestDTO.getUsername())
                        .name(userRequestDTO.getName())
                        .surname(userRequestDTO.getSurname())
                        .balance(userRequestDTO.getBalance())
                        .age(userRequestDTO.getAge())
                        .enable(userRequestDTO.getIsActive())
                        .build();
        final UserEntity userSaved = userRepository.save(userEntity);
        return UserResponseDTO
                .builder()
                .id(userSaved.getId())
                .username(userSaved.getUsername())
                .name(userSaved.getName())
                .surname(userSaved.getSurname())
                .age(userSaved.getAge())
                .balance(userSaved.getBalance())
                .isActive(userSaved.getEnable())
                .build();
    }

    @Override
    public UserResponseDTO updateUserIsActive(Long id, UserRequestDTO userRequestDTO) {

        UserEntity userEntity = userRepository.findById(id).orElseThrow();

        userEntity.setEnable(userRequestDTO.getIsActive());

        final UserEntity userSaved = userRepository.save(userEntity);
        return UserResponseDTO
                .builder()
                .id(userSaved.getId())
                .username(userSaved.getUsername())
                .name(userSaved.getName())
                .surname(userSaved.getSurname())
                .age(userSaved.getAge())
                .balance(userSaved.getBalance())
                .isActive(userSaved.getEnable())
                .build();
    }

//    @Override
//    public List<UserResponseDTO> getIsActiveUser(Boolean isActive) {
//        List<UserEntity> userEntityList = userRepository.findByEnable(true);
//        return userEntityList
//                .stream()
//                .map(user -> {
//                    return UserResponseDTO.builder()
//                            .id(user.getId())
//                            .name(user.getName())
//                            .surname(user.getSurname())
//                            .username(user.getUsername())
//                            .age(user.getAge())
//                            .isActive(user.getEnable())
//                            .balance(user.getBalance())
//                            .build();
//                }).collect(Collectors.toList());
//
//
//
//    }

//    @Override
//    public List<UserResponseDTO> getIsActiveUser(Boolean isActive) {
//        List<UserEntity> userEntity = userRepository.findByIsActiveIs(isActive);
//        return userEntity.stream().map()
//
//    }


    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Page<UserResponseDTO> getUserList(Pageable pageable) {
        return userRepository.findAll(pageable)
                .map(userEntity -> UserResponseDTO
                        .builder()
                        .id(userEntity.getId())
                        .username(userEntity.getUsername())
                        .age(userEntity.getAge())
                        .name(userEntity.getName())
                        .surname(userEntity.getSurname())
                        .isActive(userEntity.getEnable())
                        .build());
    }


}
