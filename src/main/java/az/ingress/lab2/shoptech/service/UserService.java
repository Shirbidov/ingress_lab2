package az.ingress.lab2.shoptech.service;

import az.ingress.lab2.shoptech.dto.ProductRequestDTO;
import az.ingress.lab2.shoptech.dto.ProductResponseDTO;
import az.ingress.lab2.shoptech.dto.UserRequestDTO;
import az.ingress.lab2.shoptech.dto.UserResponseDTO;
import az.ingress.lab2.shoptech.respository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface UserService {
    UserResponseDTO createUser(UserRequestDTO userRequestDTO);

    UserResponseDTO getUserByID(Long id);

    UserResponseDTO updateUser(Long id, UserRequestDTO userRequestDTO);

    void deleteUser(Long id);

    Page<UserResponseDTO> getUserList(Pageable pageable);

    UserResponseDTO updateUserIsActive(Long id, UserRequestDTO userRequestDTO);

//    UserResponseDTO getUserByName(String name);

//    UserResponseDTO getUserByIsActive(Boolean isActive);

//    List<UserResponseDTO> getIsActiveUser(Boolean isActive);





}
