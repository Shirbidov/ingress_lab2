package az.ingress.lab2.shoptech.service;

import az.ingress.lab2.shoptech.dto.ProductRequestDTO;
import az.ingress.lab2.shoptech.dto.ProductResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {
    ProductResponseDTO createProduct(ProductRequestDTO productRequestDTO);

    ProductResponseDTO updateProduct(Long id, ProductRequestDTO productRequestDTO);

    ProductResponseDTO getProductByID(Long id);

    void deleteProduct(Long id);

    Page<ProductResponseDTO> getProductList(Pageable pageable);

    ProductResponseDTO getProductByName(String name);



}
