package az.ingress.lab2.shoptech.service;

import az.ingress.lab2.shoptech.domain.ProductEntity;
import az.ingress.lab2.shoptech.dto.ProductRequestDTO;
import az.ingress.lab2.shoptech.dto.ProductResponseDTO;
import az.ingress.lab2.shoptech.exaption.NotFoundException;
import az.ingress.lab2.shoptech.respository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    final private ProductRepository productRepository;

    @Override
    public ProductResponseDTO createProduct(ProductRequestDTO productRequestDTO) {
        ProductEntity productEntity =
                ProductEntity
                        .builder()
                        .name(productRequestDTO.getName())
                        .price(productRequestDTO.getPrice())
                        .stockCount(productRequestDTO.getStockCount())
                        .build();
        final ProductEntity productSaved = productRepository.save(productEntity);
        return ProductResponseDTO
                .builder()
                .id(productSaved.getId())
                .name(productSaved.getName())
                .stockCount(productSaved.getStockCount())
                .build();
    }

    @Override
    public ProductResponseDTO updateProduct(Long id, ProductRequestDTO productRequestDTO) {
        ProductEntity productEntity =
                ProductEntity
                        .builder()
                        .id(id)
                        .name(productRequestDTO.getName())
                        .price(productRequestDTO.getPrice())
                        .stockCount(productRequestDTO.getStockCount())
                        .build();
        final ProductEntity productSaved = productRepository.save(productEntity);
        return ProductResponseDTO
                .builder()
                .id(productSaved.getId())
                .name(productSaved.getName())
                .stockCount(productSaved.getStockCount())
                .build();
    }

    @Override
    public ProductResponseDTO getProductByID(Long id) {
        final ProductEntity productEntity = productRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        return ProductResponseDTO
                .builder()
                .id(productEntity.getId())
                .name(productEntity.getName())
                .price(productEntity.getPrice())
                .stockCount(productEntity.getStockCount())
                .build();
    }

    @Override
    public void deleteProduct(Long id) {
    productRepository.deleteById(id);
    }

    @Override
    public Page<ProductResponseDTO> getProductList(Pageable pageable) {
    return productRepository.findAll(pageable)
            .map(productEntity -> ProductResponseDTO
                    .builder()
                    .id(productEntity.getId())
                    .name(productEntity.getName())
                    .price(productEntity.getPrice())
                    .stockCount(productEntity.getStockCount())
                    .build());
    }


    public ProductResponseDTO getProductByName(String name) {
         ProductEntity productEntity = productRepository.findProductByName(name).orElseThrow(()-> new RuntimeException("not found product name"));
       return ProductResponseDTO
                .builder()
                .id(productEntity.getId())
                .name(productEntity.getName())
                .price(productEntity.getPrice())
                .stockCount(productEntity.getStockCount())
                .build();
    }




}
