package az.ingress.lab2.shoptech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoptechApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoptechApplication.class, args);
	}

}
